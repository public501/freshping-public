from main_functions import *


# create command line arguments
parser = argparse.ArgumentParser(description='Choose an argument.')
parser.add_argument("--download", "-d", action="store_true", help="downloads all checks and overwrites the local check files")
parser.add_argument("--upload", "-u", action="store_true", help="uploads edited checks")
args = parser.parse_args()

if args.download:
    all_checks_creator()
    little_checks_creator()
elif args.upload:
    upload_func()
