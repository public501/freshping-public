
# Project freshping

Project freshping is a program that downloads checks from freshping and uploads any changes made to the local files.

## Configurations

Create your own auth.json file containing the api auth details. Use the following keys: passcode, user and api_url. 

##Authentication

Authentication is done over Basic Authentication using your organization API key as Username and Freshping subdomain as Password to authenticate the request.


## Usage

```python
# The command line arg -d is used to download the checks
/path/to/main.py -d

# -u is used to upload changes.
path/to/main.py -u

```

## Contributing
Section in progress...
