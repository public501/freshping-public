import os
import json
import re
import hashlib
import argparse
import requests
from requests.auth import HTTPBasicAuth
import os.path
import pathlib


# Global variables
global check_json, checks_dict, checks_files_dir, checks_files_list, passcode, user


# Generic Variables
path_to_self = pathlib.Path(__file__).parent.absolute()
path_to_self = str(path_to_self)
checks_files_dir = (path_to_self + '\\Checks')
checks_files_dir = checks_files_dir.replace('\\', '/')
headers = { 'Content-Type' : 'application/json' }
fileExtension = ".json"

try:
    checks_files_list = os.listdir(checks_files_dir)
    print("the dir Checks exists")
except:
    print("The checks directory containing all the little checks doesn't exist, run main.py with the CLA -d")


#authentication from json file
with open(path_to_self+'\\auth.json', 'r', encoding='utf-8') as f:
    auth_dict = json.load(f)
    passcode = auth_dict['passcode']
    user = auth_dict['user']
    api_url = auth_dict['api_url']
api_auth = requests.auth.HTTPBasicAuth(passcode, user)



# Functions start

# upload_func() compares the hashes of each check file with it's corresponding part of the AllChecks.json
# then if the individual check is changed uploads it to the freshping system
def upload_func():     
   
    checksdir_exist = os.path.exists(checks_files_dir)
    if checksdir_exist == False:
        print("Cannot update the files as the entire directory doesnt exist, run /path/to/main.py -d.")
        exit()

    with open('AllChecks.json', "r") as f:
        checks_dict = json.load(f)['results']

    loop_num = 0
    for file_name in checks_files_list:

        check_json = checks_dict[loop_num]
        loop_num = loop_num + 1
        id = check_json['id']
        check_path = checks_files_dir + '/' + file_name
        check_exist = os.path.exists(check_path)


        if check_exist:  #(== True)
        
            # md5 for each check file on disk
            with open(check_path, 'r') as existing_file:
                existing_file_contents = existing_file.read()
            md5_existing = hashlib.md5(existing_file_contents.encode('utf-8')).hexdigest()

            # md5 for each check in the unedited all checks file
            check_json_dumped = json.dumps(check_json) 
            md5_all_checks = (hashlib.md5(str(check_json_dumped).encode('utf-8')).hexdigest())

            # hash comparison
            if md5_all_checks == md5_existing:
                print(file_name + " doesn't need to be uploaded.")

            elif md5_all_checks != md5_existing:
                print(file_name + " needs to be uploaded (uploading...) .")
                check_url = api_url + str(id) + "/" 
                
                with open(check_path, 'rb') as fd:
                    res = requests.patch(check_url, auth=api_auth, headers=headers, data=fd)

        else:
            print("asel3")





def all_checks_creator():
    
    response = requests.get(api_url, auth=HTTPBasicAuth(passcode, user) )
    json_response = json.loads(response.content)
    json_content = json.dumps(json_response, indent=2)
    
    with open("AllChecks.json", "w", encoding="utf-8") as f:
        f.write(json_content)





def little_checks_creator():

    # Create results file
    AllChecks_file = open('AllChecks.json', "r")
    AllChecks_content = json.load(AllChecks_file)
    with open('AllResults.json', 'w') as json_file:    
        AllChecks_ResultsKey = AllChecks_content['results']
        json.dump(AllChecks_ResultsKey, json_file, indent=2)


    # check if Checks dir exists
    checks_dir_path = (checks_files_dir)
    is_exist = os.path.exists(checks_dir_path)
    if is_exist == False:
        os.mkdir(checks_dir_path)
    elif is_exist == True:
        print("Directory creation bypassed as directory already exists.")
    

    # create single check files
    for i in range(len(AllChecks_ResultsKey)):
        
        single_check_json = AllChecks_ResultsKey[i]
        id_int = single_check_json['id']
        id_str = str(id_int)

        # Create check file name - using the "for filename in checks_files_list" loop (used in the update code) won't work here
        # as there is no guarantee the single check file exists.
        completeName = os.path.join(checks_dir_path, id_str)
        fullyCompleteName = (completeName + fileExtension)
        check_exist = os.path.exists(fullyCompleteName)
        
        if check_exist == True:
           
            # md5 for each check file on disk
            with open(fullyCompleteName, 'r') as existing_file:
                existing_file_contents = existing_file.read()
            md5_existing = hashlib.md5(existing_file_contents.encode('utf-8')).hexdigest()
            print('hash for the existing file: ' + md5_existing)
           
            # md5 for each check on the freshping system
            check_json_dumped = json.dumps(single_check_json) 
            md5_all_checks = hashlib.md5(check_json_dumped.encode('utf-8')).hexdigest()
            print('hash for the straight from freshping: ' + md5_all_checks)

            #Compare the md5s   
            if md5_existing == md5_all_checks:
                print("The check with the id " + id_str + " already has an up to date version.\n")
            
            elif md5_existing != md5_all_checks:
                print("The check with the id " + id_str + " is not up to date (Downloading...)\n")
                f = open(fullyCompleteName, 'w')
                f.write(json.dumps(single_check_json))
        
        # Creates a file for each check that doesnt have one / isnt on disk
        elif is_exist == False:
            f_create = open(fullyCompleteName, "w")
            f_create.write(json.dumps(single_check_json))
            print(id_str + " does not exist but is being created.")